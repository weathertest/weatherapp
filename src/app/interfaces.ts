export interface ICurrentWeather {
  name: string
  country: string
  date: number
  temp: number
  pressure:number
  humidity: number
  weatherDescription: string
}
