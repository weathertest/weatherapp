import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing'
import { TestBed, inject, waitForAsync } from '@angular/core/testing'
import { autoSpyObj, injectClass, injectSpy } from 'angular-unit-test-helper'
import { of } from 'rxjs'

import { environment } from '../../environments/environment'
import { ICurrentWeatherData, WeatherService } from './weather.service'

const fakeWeatherData: ICurrentWeatherData = {
  country : 'IN',
  date: 1485789600,
  name: 'Ban',
  temp :0,
  pressure :0,
  humidity :0,
  weatherDescription :''

}

describe('WeatherService', () => {
  let weatherService: WeatherService

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [

      ],
    })

    weatherService = injectClass(WeatherService)
  })

  it(
    'should be created',
    waitForAsync(
      inject([WeatherService], (service: WeatherService) => {
        expect(service).toBeTruthy()
      })
    )
  )

  describe('getCurrentWeather', () => {


  })
})
