import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'

import { environment } from '../../environments/environment'
import { ICurrentWeather } from '../interfaces'

export interface ICurrentWeatherData {

      name: string
      country: string
      date: number
      temp: number
      pressure:number
      humidity: number
      weatherDescription: string


}

export const defaultWeather: ICurrentWeather = {

  name: '--',
  country: '--',
  date: Date.now(),
  temp: 0,
  pressure:0,
  humidity: 0,
  weatherDescription: '--'

}

export interface IWeatherService {
  readonly currentWeather$: BehaviorSubject<ICurrentWeather>
  getCurrentWeather(city: string): Observable<ICurrentWeather>

  updateCurrentWeather(searchText: string): void
}

@Injectable({
  providedIn: 'root',
})
export class WeatherService implements IWeatherService {
  readonly currentWeather$ = new BehaviorSubject<ICurrentWeather>(defaultWeather)

  constructor(
    private httpClient: HttpClient,
  ) {}


  updateCurrentWeather(searchText: string): void {
    this.getCurrentWeather(searchText).subscribe((weather) =>
      this.currentWeather$.next(weather)
    )
  }

  getCurrentWeather(searchText: string): Observable<ICurrentWeather> {
    const uriParams = new HttpParams()
    .set('city', searchText);


    return this.httpClient
      .get<ICurrentWeatherData>(
        `${environment.baseUrl}/weather`,
        { params: uriParams }
      )
      .pipe(map((data) => this.transformToICurrentWeather(data)))

  }



  private transformToICurrentWeather(data: ICurrentWeatherData): ICurrentWeather {
    return {

      name: data.name,
      date: Date.now(),
      temp: this.convertKelvinToFahrenheit(data.temp),
      weatherDescription: data.weatherDescription,
      country : data.country,
      pressure : data.pressure,
      humidity : data.humidity

    }
  }

  private convertKelvinToFahrenheit(kelvin: number): number {
    return (kelvin * 9) / 5 - 459.67
  }
}
