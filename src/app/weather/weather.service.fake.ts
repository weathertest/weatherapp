import { ICurrentWeather } from '../interfaces'

export const fakeWeather: ICurrentWeather = {
  name: 'Bangalore',
  country: 'IN',
  date: 1485789600,
  pressure: 0,
  humidity:0,
  temp:0,
  weatherDescription: 'light intensity drizzle',
}
